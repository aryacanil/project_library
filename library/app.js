const express = require('express')
var bodyParser = require("body-parser");

const app = express()
app.use(express.json());

const port = 3000


let firstControll=require('./controller/controller');
let details=require('./controller/books');
let id=require('./controller/idcontroller');

app.get('/books',firstControll.firstcontroller);
app.post('/insert',details.detailscontroler);
app.post('/checkkid',id.idcontroler);

app.listen(3000, () => {
  console.log(`Example app listening on port ${port}`)
})
